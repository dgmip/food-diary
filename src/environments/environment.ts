// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  endpoint: 'http://localhost:5001/meanaf-21c68/us-central1',
  firebase: {
    apiKey: 'AIzaSyAiBYO1MtBBGqPQrLWSV1OQI_gL_lihPt4',
    authDomain: 'meanaf-21c68.firebaseapp.com',
    databaseURL: 'https://meanaf-21c68.firebaseio.com',
    projectId: 'meanaf-21c68',
    storageBucket: 'meanaf-21c68.appspot.com',
    messagingSenderId: '707207657779',
    appId: '1:707207657779:web:92fb08d22c82a731c86646',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
