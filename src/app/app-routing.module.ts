import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./main/main.module').then(mod => mod.MainModule)
  },
  {
    path: 'review',
    loadChildren: () => import('./review/review.module').then(mod => mod.ReviewModule)
  },
  {
    path: 'add-food',
    loadChildren: () => import('./add-food/add-food.module').then(mod => mod.AddFoodModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
