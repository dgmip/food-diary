import { firestore } from 'firebase/app';

export interface IFoodEntry {
  created: firestore.Timestamp;
  item: string;
  amount: IFoodAmountType;
  sation: ISationType;
}

export type IFoodAmountType = 'lots' | 'some' | 'little';
export type ISationType = 'sated' | 'half' | 'hungry';
