export interface IFoodUser {
  name: string;
  email: string;
  userType: IFoodUserType;
  avatarUrl: string;
  verified: boolean;
}

export type IFoodUserType = 'user' | 'admin';
