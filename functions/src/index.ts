import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express';
import * as cors from 'cors';
// import * as cp from 'cookie-parser';
import * as bodyParser from 'body-parser';

// const validateFirebaseIdToken = async (req: any, res: any, next: any) => {
//   console.log('Check if request is authorized with Firebase ID token');
//   if (
//     (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) &&
//     !(req.cookies && req.cookies.__session)
//   ) {
//     console.error(
//       'No Firebase ID token was passed as a Bearer token in the Authorization header.',
//       'Make sure you authorize your request by providing the following HTTP header:',
//       'Authorization: Bearer <Firebase ID Token>',
//       'or by passing a "__session" cookie.',
//       req.headers
//     );
//     res.status(403).send({ message: 'Unauthorized (Bad auth header)', data: req });
//     return;
//   }
//
//   let idToken;
//   if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
//     console.log('Found "Authorization" header');
//     // Read the ID Token from the Authorization header.
//     idToken = req.headers.authorization.split('Bearer ')[1];
//   } else if (req.cookies) {
//     console.log('Found "__session" cookie');
//     // Read the ID Token from cookie.
//     idToken = req.cookies.__session;
//   } else {
//     // No cookie
//     res.status(403).send('Unauthorized (Failed to get token from session cookie)');
//     return;
//   }
//
//   try {
//     const decodedIdToken = await admin
//       .auth()
//       .verifyIdToken(idToken)
//       .catch(err => {
//         console.log('Could not verify the token', err);
//       });
//     console.log('ID Token correctly decoded', decodedIdToken);
//     req.user = decodedIdToken;
//     next();
//     return;
//   } catch (error) {
//     console.error('Error while verifying Firebase ID token:', error);
//     res.status(403).send('Unauthorized', error);
//     return;
//   }
// };

// const cookieParser = cp();
admin.initializeApp();
const app = express();

const whitelist = ['http://localhost:4200', 'http://localhost:5000', 'http://localhost:5001', 'http://localhost:4220'];
const corsOptions = {
  origin: whitelist,
};

app.get('/test', async (req: express.Request, res: express.Response) => {
  try {
    const result = 'working';
    return res.status(200).json({ message: 'Success', result });
  } catch (err) {
    console.error(err);
    return res.status(500).send(err);
  }
});

app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// app.use(cookieParser);
// app.use(validateFirebaseIdToken);

const mainApi = express();
mainApi.use('', app);
export const api = functions.https.onRequest(mainApi);

// npm i express cors cookie-parser body-parser express @types/cors @types/cookie-parser
